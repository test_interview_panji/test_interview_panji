<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Transaksi;
use App\Models\Barang;
use App\Models\Pembelian;
use Illuminate\Support\Facades\Storage;


class TransaksiController extends Controller
{
    //
    /**
     * index
     *
     * @return void
     */
    public function index()
    {

        $transaksis = Transaksi::select('transaksis.*', 'pembelians.jumlah as jumlah', 'pembelians.transaksi_pembelian_id', 'barangs.nama_barang as nama_barang', 'barangs.harga_satuan as harga_satuan')
        ->join('pembelians', 'transaksis.id', '=', 'pembelians.transaksi_pembelian_id')
        ->join('barangs', 'pembelians.master_barang_id', '=', 'barangs.id')
        ->latest()
        ->paginate(10);
        return view('transaksi', compact('transaksis'));
    }

    public function create()
    {
        $barangs    = Barang::latest()->paginate(10);
        return view('add', compact('barangs'));
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'barang' => 'required',
            'jumlah' => 'required',
            'total'  => 'required',
        ]);

        $barang = $request->barang;
        $jumlah = $request->jumlah;
        $total  = $request->total;

        $satuan = $total/$jumlah;

        $transaksi  = Transaksi::create([
            'total_harga' => $total
        ]);

        $pembelian  = Pembelian::create([
            'transaksi_pembelian_id' => $transaksi->id,
            'master_barang_id' => $barang,
            'jumlah' => $jumlah,
            'harga_satuan' => $satuan
        ]);

        if($transaksi){
            return redirect()->route('transaksi.index')->with(['success' => 'inserted']);
        }else{
            return redirect()->route('transaksi.index')->with(['error' => 'fail to insert']);
        }
    }

    public function edit($id)
    {
        $transaksis = Transaksi::select('transaksis.*', 'pembelians.jumlah as jumlah', 'pembelians.transaksi_pembelian_id', 'barangs.nama_barang as nama_barang', 'barangs.harga_satuan as harga_satuan')
        ->join('pembelians', 'transaksis.id', '=', 'pembelians.transaksi_pembelian_id')
        ->join('barangs', 'pembelians.master_barang_id', '=', 'barangs.id')
        ->latest()
        ->where('transaksis.id', '=', $id)
        ->paginate(1);
        // echo json_encode($transaksis);
        return view('edit', compact('transaksis'));
        // return view('edit', compact('content'));
    }
}
