<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Add Data</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
</head>
<body style="background: lightgray">

    <div class="container mt-5 mb-5">
        <div class="row">
            <div class="col-md-12">
                <div class="card border-0 shadow rounded">
                    <div class="card-body">
                        <form action="{{ route('transaksi.store') }}" method="POST" enctype="multipart/form-data">
                        
                            @csrf

                            <div class="form-group">
                                <label class="font-weight-bold">Barang</label>
                                <select name="barang" required onchange="tampil_total();" id="barang" class="form-control">
                                    @forelse ($barangs as $barang)
                                    <option value="{{$barang->id}}">{{$barang->nama_barang}} - {{$barang->harga_satuan}}</option>
                                    @empty
                                    @endforelse
                                </select>
                                <!-- <div class="form-check">
                                    <input class="form-check-input" type="checkbox" name="barang" harga="{{ $barang->harga_barang }}" value="{{ $barang->id }}" id="exampleCheckbox">
                                    <label class="form-check-label" for="exampleCheckbox">
                                        {{ $barang->nama_barang }}
                                    </label>
                                </div> -->


                                <!-- <input type="file" class="form-control @error('image') is-invalid @enderror" name="url"> -->
                            
                                <!-- error message untuk title -->
                                @error('barang')
                                    <div class="alert alert-danger mt-2">
                                        {{ $message }}
                                    </div>
                                @enderror
                            </div>
                            
                            <div class="form-group">
                                <label class="font-weight-bold">Kuantitas</label>
                                <input type="number" required onkeyup="tampil_total();" class="form-control @error('jumlah') is-invalid @enderror" name="jumlah" id="jumlah" value="{{ old('jumlah') }}" placeholder="Kuantitas">
                            
                                <!-- error message untuk title -->
                                @error('title')
                                    <div class="alert alert-danger mt-2">
                                        {{ $message }}
                                    </div>
                                @enderror
                            </div>

                            <div class="form-group">
                                <label class="font-weight-bold">Total</label>
                                <input type="number" required class="form-control" name="total" id="total" value="0">
                                <!-- <textarea class="form-control @error('content') is-invalid @enderror" name="desc" rows="5" placeholder="Masukkan Konten Blog">{{ old('desc') }}</textarea> -->
                            
                                <!-- error message untuk content -->
                                @error('desc')
                                    <div class="alert alert-danger mt-2">
                                        {{ $message }}
                                    </div>
                                @enderror
                            </div>

                            <button type="submit" class="btn btn-md btn-primary">SIMPAN</button>
                            <!-- <button type="reset" class="btn btn-md btn-warning">RESET</button> -->

                        </form> 
                    </div>
                </div>
            </div>
        </div>
    </div>
    
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
<script src="https://cdn.ckeditor.com/4.13.1/standard/ckeditor.js"></script>
<script>
    CKEDITOR.replace( 'content' );

    function tampil_total(){
        let kuantitas   = $("#jumlah").val();
        let barang      = $('#barang option:selected').html();
        let hargas      = barang.split(" - ");
        console.log(barang);
        let harga       = hargas[1]; 
        console.log(harga);
        let total       = kuantitas*harga;

        $("#total").val(total);
    }
</script>
</body>
</html>