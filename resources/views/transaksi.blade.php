<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Laravel CRUD</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css">
</head>
<body style="background: lightgray">

    <div class="container mt-5">
        <div class="row">
            <div class="col-md-12">
                <div class="card border-0 shadow rounded">
                    <div class="card-body">
                        <a href="{{ route('transaksi.create') }}" class="btn btn-md btn-success mb-3">Add Transkasi</a>
                        <table class="table table-bordered">
                            <thead>
                              <tr>
                                <th scope="col">Barang</th>
                                <th scope="col">Jumlah</th>
                                <th scope="col">Satuan</th>
                                <th scope="col">Total</th>
                                <th scope="col">Date</th>
                                <th scope="col">#</th>
                              </tr>
                            </thead>
                            <tbody>
                              @forelse ($transaksis as $transaksi)
                                <tr>
                                    
                                    <td>{{ $transaksi->nama_barang }}</td>
                                    <td>{{ $transaksi->jumlah }}</td>
                                    <td>{!! $transaksi->harga_satuan !!}</td>
                                    <td>{!! $transaksi->total_harga !!}</td>
                                    <td>{!! $transaksi->created_at !!}</td>
                                    <td class="text-center">
                                        <a href="{{ route('transaksi.edit', $transaksi->id) }}" class="btn btn-sm btn-primary">View</a>
                                        
                                    </td>
                                </tr>
                              @empty
                                  <div class="alert alert-danger">
                                      Data Transaksi belum Tersedia.
                                  </div>
                              @endforelse
                            </tbody>
                          </table>  
                          {{ $transaksis->links() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>

    <script>
        //message with toastr
        @if(session()->has('success'))
        
            toastr.success('{{ session('success') }}', 'BERHASIL!'); 

        @elseif(session()->has('error'))

            toastr.error('{{ session('error') }}', 'GAGAL!'); 
            
        @endif
    </script>

</body>
</html>