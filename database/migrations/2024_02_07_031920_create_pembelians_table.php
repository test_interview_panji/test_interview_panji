<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePembeliansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // 'transaksi_pembelian_id', 'master_barang_id', 'jumlah', 'harga_satuan'
        Schema::create('pembelians', function (Blueprint $table) {
            $table->id();
            $table->integer('transaksi_pembelian_id');
            $table->integer('master_barang_id');
            $table->integer('jumlah');
            $table->integer('harga_satuan');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pembelians');
    }
}
